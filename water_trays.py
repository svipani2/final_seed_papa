import picamera
import numpy as np
from PIL import Image
import re,io
import picar_4wd as fc
from picar_4wd.servo import Servo
from picar_4wd.pwm import PWM
from picar_4wd.pin import Pin
from picar_4wd.motor import Motor

import pandas as pd 
import matplotlib.pyplot as plt 
import torch
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms

from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split

import os
import time

import matplotlib.image as img

class WetDataset(Dataset):
    def __init__(self, data, path , transform = None):
        super().__init__()
        self.data = data.values
        self.path = path
        self.transform = transform
        
    def __len__(self):
        return len(self.data)
    
    def __getitem__(self,index):
        img_name,label = self.data[index]
        img_path = os.path.join(self.path, img_name)
        image = img.imread(img_path)
        if self.transform is not None:
            image = self.transform(image)
        return image, label

import torch
import torch.nn as nn
import torch.nn.functional as F

class CNN(nn.Module): 
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=10, kernel_size=3)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=3)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(720, 1024)
        self.fc2 = nn.Linear(1024, 2)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(x.shape[0],-1)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return x



CAMERA_WIDTH = 32
CAMERA_HEIGHT = 32
ser = Servo(PWM("P0"), offset=0)
pump = Motor(PWM("P8"), Pin("D11"), is_reversed=False)

while True:

    t = time.localtime()
    hour = t.tm_hour
    minute = t.tm_min
    sec = t.tm_sec

    if hour == 12 and minute == 12 and 0 < sec < 10:

        os.system('rm -r detect')
        os.system('mkdir detect')

        
        ser.set_angle(0)

        with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
            camera.start_preview()
            try:
                stream = io.BytesIO()
                camera.capture(stream, format = "jpeg")
                stream.seek(0)
                image = Image.open(stream).convert('RGB').resize(
                    (CAMERA_WIDTH, CAMERA_HEIGHT), Image.ANTIALIAS)
                
                stream.seek(0)
                stream.truncate()

            finally:
                camera.stop_preview()

        image.save(f"detect/out1.jpg", "JPEG")

        ser.set_angle(180)

        with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
            camera.start_preview()
            try:
                stream = io.BytesIO()
                camera.capture(stream, format = "jpeg")
                stream.seek(0)
                image = Image.open(stream).convert('RGB').resize(
                    (CAMERA_WIDTH, CAMERA_HEIGHT), Image.ANTIALIAS)
                
                stream.seek(0)
                stream.truncate()

            finally:
                camera.stop_preview()

        image.save(f"detect/out2.jpg", "JPEG")




        tp = r'detect/'
        train_transform = transforms.Compose([transforms.ToPILImage(),
                                              transforms.ToTensor(),])

        td = pd.DataFrame(data=[['out1.jpg', 0], ['out2.jpg', 1]], columns=['id', 'is_wet'])

        t_data = WetDataset(td, tp, train_transform )
        detect_loader = DataLoader(dataset = t_data, batch_size = batch_size, shuffle=False, num_workers=0)

        model = CNN().to(device)
        checkpoint = torch.load('model.ckpt')
        model.load_state_dict(checkpoint)


        for images, labels in detect_loader:
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)

        if predicted[0].item() == 0:
            ser.set_angle(0)
            
            # water
            print("Seed Bin 1 is dry and will be watered")
            pump.set_power(100)
            time.sleep(10)
            pump.set_power(0)

        if predicted[1].item() == 0:
            ser.set_angle(180)
            
            # water
            print("Seed Bin 2 is dry and will be watered")
            pump.set_power(100)
            time.sleep(10)
            pump.set_power(0)



        time.sleep(20)
