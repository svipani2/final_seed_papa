import os

files = os.listdir('train')

for f in files:
     if '.' in f:
             num = int(f.split('.' )[0][3:]) 
             if num < 10:
                     if num % 2 == 0:
                             os.system(f'mv train/{f} train/wet/{f}')
                     else:
                             os.system(f'mv train/{f} train/dry/{f}')
             else:
                     if num % 2 == 1:
                             os.system(f'mv train/{f} train/wet/{f}')
                     else:
                             os.system(f'mv train/{f} train/dry/{f}')
