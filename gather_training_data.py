import picamera
import numpy as np
from PIL import Image
import re,io
import picar_4wd as fc
from picar_4wd.servo import Servo
from picar_4wd.pwm import PWM

import os

m = 0
files = os.listdir('train')
for f in files:
    m = max(m, int(f.split('.')[0][3:]))

CAMERA_WIDTH = 640
CAMERA_HEIGHT = 480
ser = Servo(PWM("P0"), offset=0)
ser.set_angle(0)

with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
    camera.start_preview()
    try:
        stream = io.BytesIO()
        camera.capture(stream, format = "jpeg")
        stream.seek(0)
        image = Image.open(stream).convert('RGB').resize(
            (CAMERA_WIDTH, CAMERA_HEIGHT), Image.ANTIALIAS)
        
        stream.seek(0)
        stream.truncate()

    finally:
        camera.stop_preview()

image.save(f"train/out{m + 1}.jpg", "JPEG")

ser.set_angle(180)

with picamera.PiCamera(resolution=(CAMERA_WIDTH, CAMERA_HEIGHT), framerate=30) as camera:
    camera.start_preview()
    try:
        stream = io.BytesIO()
        camera.capture(stream, format = "jpeg")
        stream.seek(0)
        image = Image.open(stream).convert('RGB').resize(
            (CAMERA_WIDTH, CAMERA_HEIGHT), Image.ANTIALIAS)
        
        stream.seek(0)
        stream.truncate()

    finally:
        camera.stop_preview()

image.save(f"train/out{m + 2}.jpg", "JPEG")

